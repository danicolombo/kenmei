import React, {createContext, FC, useContext, useState} from 'react'
import {i18n} from '@lingui/core'

type AppContextType = {
    isShowingAlert: boolean
    handleClickContext(): void
    handleClickLenguage: (value: string) => void
    language: string
}

const AppContext = createContext<AppContextType>({
    isShowingAlert: false,
    handleClickContext: () => {},
    handleClickLenguage: () => {},
    language: ''
})

export const AppProvider: FC = ({ children }) => {
    const [isShowingAlert, setShowingAlert] = useState(false)
    const [language, setLanguage] = useState('en-us')

    const handleLanguage = (language: string) => {
        i18n.activate(language)
        setLanguage(language)
    }

    const handleClick = () => {
        setShowingAlert(true)
        setTimeout(() => {
            setShowingAlert(false)
        }, 1000)
    }

    return (
        <AppContext.Provider value={{ isShowingAlert: isShowingAlert, handleClickContext: handleClick, 
            handleClickLenguage: handleLanguage, language: language
        }}>
            {children}
        </AppContext.Provider>
    )
}

export const useAppContext = () => useContext(AppContext)