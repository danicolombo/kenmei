import React, {FC, useEffect} from 'react';
import {useAppContext} from './app-context'
import {I18nProvider as LinguiI18nProvider} from '@lingui/react';
import {i18n} from '@lingui/core';
import {messages as enUSMessages} from '../locale/en-us/messages';
import {messages as esARMessages} from '../locale/es-es/messages';
import {en, es} from 'make-plural/plurals';

i18n.loadLocaleData({
    'en-us': { plurals: en },
    'es-es': { plurals: es },
})

i18n.load({
    'es-es': esARMessages as any,
    'en-us': enUSMessages as any,
})

export const I18nProvider: FC = ({children}) => {
    const { language } = useAppContext()

    useEffect(() => {
        i18n.activate(language)
    }, [language])

    return (
        <LinguiI18nProvider i18n={i18n} key={language}>
            {children}
        </LinguiI18nProvider>
    )
}
