import React, {FC} from "react"
import {AppProvider} from "./contexts/app-context"
import {ThemeProvider} from '@material-ui/core/styles'
import {I18nProvider} from './contexts/lenguage'
import {i18n} from '@lingui/core'
import {Grid, makeStyles, Theme} from "@material-ui/core"
import LanguageHanlder from "./components/language-handler"
import AlertDisplay from "./components/alert-display"
import Button from "./components/button"
import theme from "./ui/theme"
import {messages as enUSMessages} from './locale/en-us/messages'

i18n.load('en-us', enUSMessages as any)
i18n.activate('en')

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        height: '90vh',
        alignContent: 'center',
    },
    item: {
        textAlign: 'center',
    },
}))

const App: FC = () => {
    const classes = useStyles()

    return (
        <AppProvider>
            <I18nProvider>
                <ThemeProvider theme={theme}>
                    <Grid container>
                        <Grid item xs={12}>
                            <LanguageHanlder />
                        </Grid>
                    </Grid>
                    <Grid container justify='center' className={classes.root}>
                        <Grid item md={6} xs={12} className={classes.item}>
                            <AlertDisplay />
                            <Button />
                        </Grid>
                    </Grid>
                </ThemeProvider>
            </I18nProvider>
        </AppProvider>
    )
}

export default App
