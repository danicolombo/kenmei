
import {createMuiTheme} from '@material-ui/core/styles'
import purple from '@material-ui/core/colors/purple'

const theme = createMuiTheme({
    palette: {
        primary: {
            main: purple[500],
        },
        background: {
            default: "#0B72B9",
        }
    },
    typography: {
        h4: {
            fontWeight: 300,
        },
    }
})


export default theme
