import React, {FC} from "react"
import {useAppContext} from '../contexts/app-context'
import {Grid, makeStyles, Theme} from "@material-ui/core"
import IconButton from '@material-ui/core/IconButton';

import {
    IconFlagES,
    IconFlagUS
} from 'material-ui-flags';

const useStyles = makeStyles((theme: Theme) => ({
    wrapper: {
        fontSize: '18px',
        display: 'flex',
        msFlexDirection: 'row',
        justifyContent: 'center',
    },
}))

const LanguageHanlder: FC = () => {
    const { handleClickLenguage } = useAppContext()
    const classes = useStyles()

    return (
        <Grid container justify='center'>
            <Grid item xs={12} className={classes.wrapper}>
                <IconButton onClick={() => handleClickLenguage('es-es')}><IconFlagES /></IconButton>
                <IconButton onClick={() => handleClickLenguage('en-us')}><IconFlagUS /></IconButton>
            </Grid>
        </Grid>
    )
}

export default LanguageHanlder
