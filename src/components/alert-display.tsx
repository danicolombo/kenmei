import React, {FC} from "react"
import {useAppContext} from '../contexts/app-context'
import {Trans} from '@lingui/macro'
import {Alert, AlertTitle} from '@material-ui/lab'
import {Grid, Theme, makeStyles, Typography} from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) => ({
    shown: {
        fontSize: '18px',
        margin: theme.spacing(2),
        textAlign: 'left',
        height: '100px',
    },
    hidden: {
        opacity: 0,
        transition: 'all 250ms linear 2s',
        margin: theme.spacing(2),
        textAlign: 'left',
        height: '100px',
    },
}))

const AlertDisplay: FC = () => {
    const { isShowingAlert } = useAppContext()
    const classes = useStyles()

    return (
        <Grid container justify='center'>
            <Grid item xs={12}>
                <Alert severity="success" className={isShowingAlert ? classes.shown : classes.hidden}>
                    <AlertTitle>Success</AlertTitle>
                    <Typography variant='h5'><Trans>This is a success alert! I'm glad to see you here</Trans> ʕ•́ᴥ•̀ʔ</Typography>
                </Alert>
            </Grid>
        </Grid>
    )
}

export default AlertDisplay
