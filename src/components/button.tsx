import React, {FC} from "react"
import {useAppContext} from '../contexts/app-context'
import {Trans} from '@lingui/macro'
import {Button as MuiButton, makeStyles, Theme} from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) => ({
    button: {
        padding: theme.spacing(2),
    },
}))

const Button: FC = () => {
    const { handleClickContext } = useAppContext()
    const classes = useStyles()

    return (
        <MuiButton
            variant="contained"
            color="primary"
            size="large"
            className={classes.button}
            onClick={() => handleClickContext()}>
            <Trans>Show alert</Trans>
        </MuiButton>
    )
}

export default Button
