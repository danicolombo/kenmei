# Kenmei

It's an open source project developed with React 17, Typescript, Material ui, React Context API.

## Build

Inside the ui directory, install the dependencies (with Node version major 14):

$ npm install

Initialize webpack:

$ npm run dev

## Frameworks & libraries

* [React](https://es.reactjs.org/) 
* [Typescript](https://www.typescriptlang.org/) 
* [Material](https://material-ui.com/) 
* [React Context API](https://reactjs.org/docs/context.html) 
* [i18n](https://www.npmjs.com/package/i18n) 

## Author

* **Daniela Colombo** - [Portfolio](https://procnedc.github.io/resume)

## Licence

MIT [licence](https://opensource.org/licenses/MIT) for more details.

